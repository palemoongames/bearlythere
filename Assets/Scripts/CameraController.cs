﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform target;
	public float distance = 5.0f;

	public float yOffset = 3;

	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;

	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;

	public float distanceMin = .5f;
	public float distanceMax = 15f;

	//private Rigidbody rigidbody;

	float x = 0.0f;
	float y = 0.0f;

	void Start () 
	{
		//Vector3 angles = transform.eulerAngles;
		//x = angles.y;
		//y = angles.x;

		//rigidbody = GetComponent<Rigidbody>();

		// Make the rigid body not change rotation
		//if (rigidbody != null)
		//{
		//	rigidbody.freezeRotation = true;
		//}
	}

	void LateUpdate () 
	{
		if (target) 
		{
			//x += Input.GetAxis("Mouse X") * xSpeed * distance * 0.02f;
			//y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

			//y = ClampAngle(y, yMinLimit, yMaxLimit);

			//Quaternion rotation = Quaternion.Euler(y, x, 0);

			distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel")*5, distanceMin, distanceMax);

			/*RaycastHit hit;
			if (Physics.Linecast (target.position, transform.position, out hit)) 
			{
				if (hit.collider.tag != "Player")
					distance -= hit.distance;
				else
					return;
			}*/

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			Vector3 targetPos = target.position;
			Vector3 position = negDistance + target.position;

			//rotation = Quaternion.Euler(y-5, x, 0);

			//transform.rotation = rotation;
			transform.position = Vector3.Lerp(transform.position, position, 1);
		}
	}

	//public static float ClampAngle(float angle, float min, float max)
	//{
		//if (angle < -360F)
			//angle += 360F;
		//if (angle > 360F)
			//angle -= 360F;
		//return Mathf.Clamp(angle, min, max);
	//}
}