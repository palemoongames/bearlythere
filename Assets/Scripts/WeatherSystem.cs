﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherSystem : MonoBehaviour {
 	
	public Transform target;
	public Material water,water2;

	public float waterSpeed = 1;

	public CoreLoop core;

	public GameObject rain;
	public GameObject leaves;
	public GameObject snow;
    AudioSource audioS;
    public AudioClip[] soundLoop;

    public float springAmbVolume;
    public float summerAmbVolume;
    public float autumnAmbVolume;
    public float winterAmbVolume;
    public GameObject[] soundPlayers;

	Vector2 offset = new Vector2 ();

	public Light sunlight;

	public Material[] skyboxes;
	public Color[] sunlightColours;

    void Start(){
        audioS = GetComponent<AudioSource>();
        UpdateWeather();
        foreach (GameObject go in soundPlayers) {
            go.SetActive(true);
        }

    }

    void Update () {
		UpdateWater ();
	}

	void LateUpdate () {
		Vector3 pos = target.position;
		pos.y += 10;
		transform.position = pos;
	}

	void UpdateWater () {

		offset.y += Time.deltaTime * waterSpeed;
		offset.x += Time.deltaTime * waterSpeed/1.5f;
		if (offset.y > 1) {
			offset.y = 0;
		}
		if (offset.x > 1)
		{
			offset.x = 0;
		}
		water.mainTextureOffset = new Vector2(0, offset.y);
		water.SetTextureOffset ("_DetailAlbedoMap", new Vector2(0.5f, offset.x));
		water2.mainTextureOffset = new Vector2(0, offset.y);
		water2.SetTextureOffset("_DetailAlbedoMap", new Vector2(0.5f, offset.x));
	}

	public void UpdateWeather(){
		if (core.season == Season.Spring) {
			snow.SetActive (false);
            rain.SetActive (true);
			audioS.Stop();
			audioS.volume = springAmbVolume;
            audioS.clip = soundLoop[0];
            audioS.Play();
			RenderSettings.skybox = skyboxes[0];
			sunlight.color = sunlightColours [0];

		} else if (core.season == Season.Summer) {
			rain.SetActive (false);
			audioS.volume = summerAmbVolume;
			RenderSettings.skybox = skyboxes[1];
			sunlight.color = sunlightColours [1];
		} else if (core.season == Season.Autumn) {
			leaves.SetActive (true);
            audioS.Stop();
            audioS.clip = soundLoop[1];
			audioS.volume = autumnAmbVolume;
            audioS.Play();
			RenderSettings.skybox = skyboxes[2];
			sunlight.color = sunlightColours [2];
		} else if (core.season == Season.Winter) {
			leaves.SetActive (false);
			snow.SetActive (true);
			audioS.Stop();
			audioS.volume = winterAmbVolume;
			audioS.clip = soundLoop[1];
			audioS.Play();
			RenderSettings.skybox = skyboxes[3];
			sunlight.color = sunlightColours [3];
		}
	}
}
