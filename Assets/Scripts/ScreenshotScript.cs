using UnityEngine;
using System.Collections;
using System.IO;

public class ScreenshotScript : MonoBehaviour{
    Texture2D screenCap;
    int screenshotCount;
    string filename;
    DirectoryInfo filePath = new DirectoryInfo(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/BearlyThere Screenshots/");

    void Start(){
        //Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        // check for/create screenshot folder
        if (!Directory.Exists(filePath.FullName)){
            Directory.CreateDirectory(filePath.FullName);
            return;
        }

        if (Directory.GetFiles(filePath.FullName).Length > 0){
            // offset count by number of exisiting screenshots in the folder
            screenshotCount = Directory.GetFiles(filePath.FullName).Length;

            FileInfo[] tmpFile = filePath.GetFiles();
            byte[] fileData;
            fileData = File.ReadAllBytes(tmpFile[tmpFile.Length-1].FullName);
           
            //screenCap = new Texture2D(2, 2);
            //screenCap.LoadImage(fileData); //..this will auto-resize the texture dimensions.

            //GetComponent<Renderer>().material.mainTexture = screenCap;
            //Destroy(texture);
        }
    }
    
    void Update(){
        if (Input.GetButtonDown("Screenshot")){
            StartCoroutine(ScreenshotEncode());
        }
    }

    IEnumerator ScreenshotEncode(){
        // wait for graphics to render
        yield return new WaitForEndOfFrame();

        // put buffer into texture
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBAFloat, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        // split the process up--ReadPixels() and the GetPixels() call inside of the encoder are both pretty heavy
        yield return 0;
        byte[] bytes = texture.EncodeToPNG();
        
        // save our test image (could also upload to WWW)
        filename = "BearlyThereScreenshot" + screenshotCount + ".png";
        File.WriteAllBytes(filePath + filename, bytes);
        screenshotCount++;
        
        byte[] fileData;
        fileData = File.ReadAllBytes(filePath + filename);
        //screenCap = new Texture2D(2, 2);
        //screenCap.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        
        //GetComponent<Renderer>().material.mainTexture = screenCap;
        Destroy(texture);
    }
}