﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearSplashSound : MonoBehaviour {

    public AudioClip[] bearSplash;
    AudioSource audioS;
    Rigidbody rb;
    private GameObject splashParticle;

    void Start() {
        audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        splashParticle = (GameObject)Resources.Load("Splash");
    }
    void Update(){
        if (!audioS) {
            audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
        }
    }

    void OnTriggerEnter(Collider hit) {
        if (hit.tag == "Water" && !audioS.isPlaying){
            if (rb.velocity.magnitude > 0){ 
                int rn = Random.Range(0, bearSplash.Length);
                audioS.PlayOneShot(bearSplash[rn]);
                GameObject tmpGO = (GameObject)Instantiate(splashParticle, transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));
                Destroy(tmpGO, 1);
            }
        }
    }

    public void ResetAudioSource(){
        audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
    }
}


