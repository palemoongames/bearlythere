﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearSounds : MonoBehaviour {

	public AudioClip[] bearSounds;
	float timer;
	int rn;
    private CoreLoop core;
    public EatScript eat;

	// Use this for initialization
	void Start () {
		timer = 0;
		rn = Random.Range(0,10);
        core = Camera.main.GetComponent<CoreLoop>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!core.gameOver) {
            timer += Time.deltaTime;
            if (timer > (5 + rn)) {
                rn = Random.Range(0, 10);
                timer = 0;
                GetComponent<AudioSource>().PlayOneShot(bearSounds[Random.Range(0, bearSounds.Length)]);
                eat.Chomp(1);
            }
        }
	}
}
