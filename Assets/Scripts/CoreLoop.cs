﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using Cinemachine;
using TMPro;

public enum Season {
	Spring,
	Summer,
	Autumn,
	Winter,
}

public class CoreLoop : MonoBehaviour {

    public UIController ui;
	public GameObject canv;

	public Season season;
	public GameObject bearPrefab;
	private GameObject bear;

	public GameObject spawn;
	public Image overlay;

	public float endTime = 300;
	private float currentTime = 0;
	private float endTimer = 0;
    private float clockTime;
	public float timeToFade = 2;

	public float goalFood = 1;
	public float currentFood = 0.05f;

	public Animator stummyAnim;
	private float pangTimer;

	public bool gameStarted = false;
	public bool gameOver;
	private bool isWinter;
	private bool inCave;
	public bool isDead;
	public WeatherSystem weather;
	private int seasonNumber = 4;

	public AudioClip death;
    private bool deathSoundPlayed = false;
	public GameObject sleep;

	public Vector3 lastPosOnGround;

	public GameObject titleCam;
	public GameObject bearCam;
	private float camTimer;
	public bool intro = false;
    private bool caveNag = true;
	public float holdTimer = 1;
    public int tutFlag;

	private void Awake(){
		overlay.DOColor(new Color(1,1,1,0), timeToFade);
		canv.SetActive(true);
        AudioListener.volume = 1f;
	}

	void Start(){
		season = Season.Winter;
		weather.UpdateWeather();
	}

	void Update(){
		if (!gameStarted){
			if (Input.anyKeyDown){
				intro = true;
				gameStarted = true;
				titleCam.SetActive(true);
			}
			return;
		}

		if (intro){
            sleep.SetActive(false);
            camTimer += Time.deltaTime;
			if (camTimer >= 2+holdTimer){
				SpawnBear(spawn.transform.position);
				season = Season.Spring;
				weather.UpdateWeather();
				intro = false;
                caveNag = true;
                tutFlag = 0;
			}
		}

		pangTimer += Time.deltaTime;
		if(pangTimer >= currentFood * 50 && currentFood <= goalFood){
			pangTimer = 0;
			stummyAnim.SetTrigger("Pang");
		}

		stummyAnim.SetBool("Dead", isDead);

		if (Input.GetButtonDown("Restart")) {
			SceneManager.LoadScene (0);
		}

		currentTime += Time.deltaTime;
        clockTime = Mathf.Round((endTime - currentTime) - (endTime / 4));
        if (clockTime >= 0)
        {
            canv.GetComponent<TextMeshProUGUI>().text = ("" + Mathf.Floor(clockTime / 60));
            if (clockTime % 60 < 10)
            {
                canv.GetComponent<TextMeshProUGUI>().text += (":0" + (clockTime % 60));
            } else
            {
                canv.GetComponent<TextMeshProUGUI>().text += (":" + (clockTime % 60));
            }
        } else
        {
            canv.GetComponent<TextMeshProUGUI>().text = ("0:00");
        }
            


		if (currentTime < endTime / 4) {
			if (seasonNumber == 4){
				seasonNumber = 0;
				season = Season.Spring;
				weather.UpdateWeather();
				seasonNumber++;
                ui.DisplayMessage(0);
			}
		} else if (currentTime < endTime / 4 * 2) {
			if (seasonNumber == 1){
				season = Season.Summer;
				weather.UpdateWeather();
				seasonNumber++;
                ui.DisplayMessage(1);
            }
		} else if (currentTime < endTime / 4 * 3) {
			if (seasonNumber == 2){
				season = Season.Autumn;
				weather.UpdateWeather();
				seasonNumber++;
                ui.DisplayMessage(2);
            }
		} else if (currentTime < endTime) {
			if (seasonNumber == 3){
				season = Season.Winter;
				weather.UpdateWeather();
				seasonNumber = 4;
			}
		}

		if (currentTime >= endTime - (endTime/4)){
			print ("Times Up Bear");
			if (currentFood >= goalFood && inCave) {
				print ("Barris is sheltered and fed! NICE");
				gameOver = true;
                ui.DisplayMessage(9);
			} else if (currentFood < goalFood && inCave) { 
				isDead = true;
				gameOver = true;
				print ("Barris is in the cave but didn't he enough, he perished over the winter! :(");
                int rn = Random.Range(7, 9);
                if (ui.currentMessage == 10) {
                    ui.DisplayMessage(rn);
                }
			} else if (!inCave){
                isDead = true;
                gameOver = true;
                print("Barris died out in the cold! :(");
                int rn = Random.Range(5, 7);
                if (ui.currentMessage == 10) {
                    ui.DisplayMessage(rn);
                }
            }
		}

        switch (tutFlag)
        {
            case 0:
                if (currentTime > 10 && ui.currentMessage == 10)
                {
                    ui.DisplayMessage(15);
                    tutFlag++;
                }
                break;
            case 1:
                if (currentTime > 10 && ui.currentMessage == 10)
                {
                    ui.DisplayMessage(16);
                    tutFlag++;
                }
                break;
            case 2:
                if (currentTime > 10 && ui.currentMessage == 10)
                {
                    ui.DisplayMessage(17);
                    tutFlag++;
                }
                break;
        }
        
        if (gameOver == true) {
            endTimer += Time.deltaTime;
            bearCam.GetComponent<CinemachineFreeLook>().m_YAxis.m_MaxSpeed = 0;
            bearCam.GetComponent<CinemachineFreeLook>().m_XAxis.m_MaxSpeed = 0;

            if (!deathSoundPlayed) {
                bear.GetComponent<AudioSource>().PlayOneShot(death);
                deathSoundPlayed = true;
            }

            if (endTimer >= ui.messageLength) {
                canv.transform.GetChild(0).gameObject.SetActive(false);
            }

            if (endTimer >= ui.messageLength + 1){
                overlay.DOColor(Color.black, timeToFade);
                AudioListener.volume -= 0.02f;
            }

			if (endTimer >= timeToFade + ui.messageLength + 3){
				SceneManager.LoadScene(0);
			}
		}
	}

	public void SpawnBear (Vector3 pos) {
		bear = (GameObject)Instantiate (bearPrefab, pos, spawn.transform.rotation);
		bear.name = "Bear";
        bear.GetComponentInChildren<BearSplashSound>().ResetAudioSource();
		bear.GetComponentInChildren<BearController> ().ApplyForce (spawn.transform.TransformDirection (Vector3.forward) * -7, ForceMode.Impulse);
		Camera.main.GetComponent<CameraController> ().target = bear.GetComponentInChildren<BearController> ().gameObject.transform;
		if (lastPosOnGround != Vector3.zero) {
			bear.GetComponentInChildren<BearController> ().lastPosOnGround = lastPosOnGround;
		}
		bearCam.GetComponent<CinemachineFreeLook>().m_Follow = bear.transform;
		bearCam.GetComponent<CinemachineFreeLook>().m_LookAt = bear.transform;
		bearCam.SetActive(true);
    }

	public void AddFood (float food) {
		currentFood += food;
	}

	public float GetFood () {
		if (currentFood > goalFood) {
            if (caveNag)
            {
                ui.DisplayMessage(14);
                caveNag = false;
            }
			return 1;
		} else {
			return currentFood;
		}
	}

	public void EnterCave () {
		sleep.SetActive (true);
        if (season == Season.Winter) {
            inCave = true;
            currentTime = endTime;
        } else if (season == Season.Summer || season == Season.Autumn) {
            if (currentFood >= goalFood) {
                inCave = true;
                currentTime = endTime;
            } else {
                inCave = true;
                ui.DisplayMessage(13);
            }
        } else if (season == Season.Spring) {
            if (currentTime > 6f) {
                ui.DisplayMessage(11);
            }
        }
	}
	public void ExitCave () {
		sleep.SetActive (false);
	}
}
