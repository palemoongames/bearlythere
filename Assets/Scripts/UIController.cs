﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour {

	public CoreLoop core;

	public Slider slider;

	public Image title;
	public Image logo;

    public float messageLength;
	public TMP_Text text;
	private string message;

	public int currentMessage = 0;

	public string[] messages;

	private float timer;
	private bool displayingMessage;

	void Awake () {
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		text.gameObject.SetActive(false);
		slider.gameObject.SetActive (false);
        currentMessage = 10;
	}

	void Update () {
		timer += Time.deltaTime;

        //QUIT GAME
        if (Input.GetKeyDown(KeyCode.P)){
            Application.Quit();
        }

		if(Input.GetKeyDown(KeyCode.DownArrow) && currentMessage > 0){
			currentMessage --;
		}
		if (Input.GetKeyDown (KeyCode.UpArrow) && currentMessage < (messages.Length-1)) {
			currentMessage++;
		}
		if (core.gameStarted) {
			title.enabled = false;
			logo.enabled = false;
			text.gameObject.SetActive(true);
			slider.gameObject.SetActive(true);
		}

		slider.value = core.GetFood ();
        
        if (timer > messageLength) {
            DisplayMessage(10);
        }

	}

	public void DisplayMessage (int n) {
        currentMessage = n;
        text.text = messages[currentMessage];
        timer = 0;
	}
}
