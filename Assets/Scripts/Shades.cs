﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shades : MonoBehaviour
{
   void OnTriggerEnter(Collider hit) {
        if (hit.gameObject.name == "Cylinder001") {
            GameObject.Find("Shades").GetComponent<MeshRenderer>().enabled = true;
            Destroy(gameObject);  
        }
    }
}
