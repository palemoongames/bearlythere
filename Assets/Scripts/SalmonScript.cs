﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalmonScript : MonoBehaviour {

	public AudioClip[] splash;
	public AudioClip[] wetSlap;
	AudioSource audioS;
	private GameObject splashParticle;

	private float timer;
	private float timeToJump = 10;
	private float jumpModifier = 4;
	private float jumpForce = 30;

	public Vector3 jumpDir;

	private Rigidbody body;

	private bool canJump;

	void Start () {
		audioS = GetComponent<AudioSource>();
		splashParticle = (GameObject)Resources.Load ("Splash");
		body = GetComponent<Rigidbody> ();
	}

	void Update () {
		timer += Time.deltaTime;
		if (timer >= timeToJump + Random.Range (-jumpModifier + 5, jumpModifier) && canJump) {
			
			timer = 0;
			canJump = false;
			body.AddForce (jumpDir * (jumpForce+Random.Range(-jumpModifier*2, jumpModifier*2)), ForceMode.Impulse);
		}
		RaycastHit hit = new RaycastHit ();
		if (Physics.Raycast (transform.position, Vector3.down, out hit)) {
			if(hit.distance < 0.5){
				canJump = true;
			} else {
				canJump = false;
			}
		}
	}

	void OnTriggerEnter(Collider hit){
		if (hit.tag == "Water" && !audioS.isPlaying) {
			int rn = Random.Range(0,splash.Length);
			audioS.PlayOneShot(splash[rn]);
			GameObject tmpGO = (GameObject)Instantiate (splashParticle, transform.position, Quaternion.Euler (new Vector3(-90,0,0)));
			Destroy (tmpGO,5);
		}
        
        if (hit.tag == "Bear") {
			int rn2 = Random.Range(0,wetSlap.Length);
			audioS.PlayOneShot(wetSlap[rn2]);
			print ("wet slap " + rn2);
		}
	}
}
