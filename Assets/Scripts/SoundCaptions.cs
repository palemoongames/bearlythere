﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCaptions : MonoBehaviour {

	public Sprite[] captions;
	private SpriteRenderer rend;
	private Animation anim;

	void Awake () {
		rend = GetComponent<SpriteRenderer> ();
		int rn = Random.Range (0, captions.Length);
		rend.sprite = captions [rn];
	}
}
