﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatScript : MonoBehaviour {

	private CoreLoop core;

	public Rigidbody jawBody;
	public Rigidbody headBody;
	AudioSource audioS;
	public AudioClip[] eatSound;
	private GameObject sCaption;
    private bool firstBerry = true;
    private UIController ui;

	public float jawForce = 2;
	public float berryValue;
	public float salmonValue;
    private Transform cam;

	void Start(){
        ui = Camera.main.GetComponent<UIController>();
		core = Camera.main.GetComponent<CoreLoop> ();
		audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
		sCaption = GameObject.Find ("SoundCaptionSpawn");
        cam = GameObject.FindWithTag("MainCamera").transform;
	}

	void Update () {
		if (audioS == null) {
			audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
		}
		if (Input.GetButtonDown("Fire1") && !core.gameOver) {
            Chomp(3);
		}
		if (!audioS){
            audioS = GameObject.Find("Bear").GetComponent<AudioSource>();
        }
	}

	void OnTriggerStay (Collider hit){
 		if (Input.GetButtonDown("Fire1")) {
 			if (hit.tag == "Salmon" || hit.tag =="Berry") {
                hit.SendMessageUpwards ("Eaten");
				int rn = Random.Range(0,eatSound.Length);
				audioS.PlayOneShot(eatSound[rn]);
				if (hit.tag == "Berry"){
					core.AddFood(berryValue);
                    if (firstBerry && ui.currentMessage == 10)
                    {
                        ui.DisplayMessage(12);
                        firstBerry = false;
                    }
                } else if (hit.tag == "Salmon"){
					core.AddFood (salmonValue);
				}
				if (GameObject.FindWithTag ("SCap") == null) {
					GameObject sc = (GameObject)Instantiate (Resources.Load ("SoundCaption"), sCaption.transform);
					Destroy (sc, 2);
				}
			}
		}
	}

    public void Chomp(float forwardForce) {
        jawBody.AddForceAtPosition(jawBody.transform.TransformDirection(Vector3.left) * jawForce, jawBody.transform.position, ForceMode.Impulse);
        headBody.AddForceAtPosition(headBody.transform.TransformDirection(Vector3.up) * jawForce, headBody.transform.position, ForceMode.Impulse);
        headBody.AddForceAtPosition(cam.TransformDirection(Vector3.forward) * jawForce * forwardForce, headBody.transform.position, ForceMode.Impulse);
    }
}
