﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalmonCannon : MonoBehaviour {

	public GameObject salmon;

	public float spawnRate;
	public float spawnModifier;

	public float forceBase;
	public float forceModifier;

	public Vector2 spawnZone;

	private float timer;

	void Update () {
		timer += Time.deltaTime;
		if (timer >= spawnRate + Random.Range (-spawnModifier, spawnModifier)) {
			FireSalmon (GenerateSpawnPos ());
			timer = 0;
		}
	}

	void FireSalmon (Vector3 pos) {
		GameObject tmpGO = (GameObject)Instantiate (salmon, pos, transform.rotation);
		Rigidbody tmpBody = tmpGO.GetComponentInChildren<Rigidbody> ();
		Vector3 tmpDir = transform.TransformDirection (Vector3.forward);
		tmpGO.GetComponentInChildren<SalmonScript> ().jumpDir = tmpDir;
		tmpBody.AddForce(tmpDir*(forceBase +Random.Range(-forceModifier,forceModifier)),ForceMode.Impulse);
	}

	Vector3 GenerateSpawnPos () {
		Vector3 tmpPos = transform.position + new Vector3 (Random.Range (-spawnZone.x, spawnZone.x), 0, Random.Range (-spawnZone.y, spawnZone.y));
		return tmpPos;
	}
}
