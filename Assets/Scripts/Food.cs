﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {

	void Start () {
		if(this.tag == "Salmon")
			Destroy (gameObject, 200);
	}

    public void Eaten(){
		Destroy (gameObject);
	}
}
