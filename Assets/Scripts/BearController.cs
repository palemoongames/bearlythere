﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearController : MonoBehaviour {

	private CoreLoop core;
	private Transform camTransform;
	private Rigidbody body;

	public float speed = 40;
	public float jumpForce = 2;
	public float lift = 0.75f;
    public float pushForce;

	private bool canJump;
    private float invert;
	private float timer;

	public Vector3 lastPosOnGround;

	void Awake () {
        invert = 1;
		core = Camera.main.GetComponent<CoreLoop> ();
		body = GetComponent<Rigidbody> ();
	}
    void Start() {
        camTransform = GameObject.FindWithTag("MainCamera").transform;
    }
	
	void Update () {
		if (core.gameOver) {
			return;
		}
        if (Input.GetKeyDown(KeyCode.Alpha9)) {
            invert = -invert;
        }

		Vector3 movement = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
		movement = camTransform.TransformDirection (movement);
		movement.y = lift;

		if (!canJump) {
			RaycastHit hit = new RaycastHit();
			if(Physics.Raycast(transform.position, Vector3.down, out hit)){
				if (hit.distance <= 1.25f) {
					canJump = true;
					lastPosOnGround = hit.point;
				}
			}
		}

		if (Input.GetButtonDown("Jump") && canJump){
			ApplyForce(Vector3.up*jumpForce, ForceMode.Impulse);
            ApplyForce(new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z) *pushForce, ForceMode.Impulse);
            canJump = false;
		}

		//ApplyForce(movement, ForceMode.Force);
	}

	public void ApplyForce (Vector3 direction, ForceMode fMode) {
		body.AddForce (direction*speed, fMode);
	}

	void OnTriggerEnter (Collider hit ) {
		if (hit.tag == "Bounds") {
			Destroy (GameObject.FindWithTag("GameController"));
			core.SpawnBear (lastPosOnGround + (Vector3.up *2));
			core.lastPosOnGround = lastPosOnGround;
		}
	}

	void OnTriggerStay (Collider hit) {
		if (hit.tag == "Cave") {
			core.EnterCave ();
		}
	}

	void OnTriggerExit (Collider hit) {
		if (hit.tag == "Cave") {
			core.ExitCave ();
		}
	}
}
